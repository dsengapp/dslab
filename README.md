# DSLab - Data Science Laboratory

Refactured code from Data Science experiments, projects, and lectures developed at the Department of Engineering Science.

## Setup using VirtualEnv with Anaconda

    git clone git@bitbucket.org:dsengapp/dslab.git
    cd dslab
    conda create --name dslab python=3.10 -y
    conda activate dslab
    conda install -y -c anaconda -c conda-forge --file=requirements.txt
    
    pip install -e .
    ipython kernel install --user --name=dslab
    python -c 'import dslab' # Check installation

## Deinstallation of virtual environment

    conda remove --name dslab --all
    jupyter kernelspec uninstall dslab # No capital letters!!
    
## Citation

```
@misc{DSlab,
  author = {Andreas W. Kempa-Liehr and Alex Kennedy and Ivan Koptev and Gemma Nash},
  title = {DSLab - Data Science Laboratory},
  year = {2017--2021},
  note = {\url{https://bitbucket.org/dsengapp/dslab}}
  }
```



## Note

This project has been set up using PyScaffold 3.0.3. For details and usage
information on PyScaffold see http://pyscaffold.org/.
