=========
Changelog
=========

Version 0.5
===========
- Add example for visualizing Bayesian regression models inclusive highest density intervals
- Breaking change: Method for computing posterior predictive distribution renamed to .predict_ppd()

Version 0.4
===========

- FIX: Plotting of copula

