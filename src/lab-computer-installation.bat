rem @echo off
echo D-drive installation of Anaconda 64-bit extra add-ins for ENGSCI762 and ENGSCI712
echo Web Link: https://anaconda.org/conda-forge/tsfresh
echo Contact of Academic: Dr. Kempa-Liehr (Engsci)
echo ==============================================================
echo Assume existing installation of Anaconda Python 3.7.5 (Anaconda3  64 bit) through SCCM
echo ===============================================================
echo Create local d-drive folder 
mkdir d:\python_term120x

echo Add Git to search path in order to clone repository. This is also needed for installing dslab (line 31)
set PATH=%PATH%;C:\Program Files\Git\cmd

echo Clone dslab.git into local d-drive folder
D:
cd d:\python_term120x
git clone --single-branch --branch term120x https://stash.auckland.ac.nz/scm/dsea/dslab.git dslab_term120x

echo Activate Anaconda
call C:\Anaconda\Scripts\activate.bat C:\Anaconda

echo Create local Python environment on D-drive and install dslab module including dependencies
rem The following command needs admin rights otherwise an interactive dialog demands authentification and the script stops if unsuccessful.
conda create --prefix d:\python_term120x\envs python=3.7 -y -c anaconda -c conda-forge --file=dslab_term120x\requirements.txt
conda activate d:\python_term120x\envs
conda install -y conda-build
cd dslab_term120x

rem The following command requires that git is included in the search path,
rem which is a requirement of module pyscaffold.
python setup.py develop
conda develop .
conda activate d:\python_term120x\envs

echo Test installation of dslab
python -c "import dslab"
