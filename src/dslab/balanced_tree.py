import numpy as np
from sklearn.linear_model import LogisticRegression


class ClassifierTree:
    def __init__(self, classifier=LogisticRegression, max_level=4, level=0):
        self.classifier = classifier
        self.max_level = max_level
        self.level = level

    def fit(self, x, y):
        self.clf = self.classifier()
        self.median = np.median(y)
        self.min = np.min(y)
        self.max = np.max(y)
        #print(self.level, self.median)
        self.mask = y >= self.median
        if len(x.shape) == 1:
            self.clf.fit(x[:, np.newaxis], self.mask)
        else:
            self.clf.fit(x, self.mask)

        if self.level < self.max_level:
            self.right = ClassifierTree(self.classifier,
                                        max_level=self.max_level,
                                        level=self.level + 1)
            self.right.fit(x[self.mask], y[self.mask])
            self.left = ClassifierTree(self.classifier,
                                       max_level=self.max_level,
                                       level=self.level + 1)
            self.left.fit(x[~self.mask], y[~self.mask])
        self.estimators_ = self.get_estimators_()

    def predict_proba(self, X):
        quantiles = list()
        self._quantiles(X, quantiles, np.ones(X.shape[0]))
        return self.bins(), np.array(quantiles)

    def _quantiles(self, X, quantiles, prior):
        likelihood = self.clf.predict_proba(X)
        if self.level < self.max_level - 1:
            self.left._quantiles(X, quantiles, prior * likelihood[:, 0])
            self.right._quantiles(X, quantiles, prior * likelihood[:, 1])
        else:
            quantiles.append(prior * likelihood[:, 0])
            quantiles.append(prior * likelihood[:, 1])

    def _flatten(self, l):
        return np.array(l).flatten().tolist()

    def _bins(self):
        if self.level < self.max_level - 1:
            result = self._flatten(self.left._bins())
            result.append(self.median)
            result += self._flatten(self.right._bins())
            return result
        else:
            return self.median

    def bins(self):
        bins = [self.min] + self._flatten(self._bins())
        bins.append(self.max)
        return bins

    def predict_pdf(self, X):
        bins, height = self.predict_proba(X)

        width = np.diff(bins)
        center = bins[0:-1] + width / 2
        return center, height, width

    def get_estimators_(self, estimators_=None):
        """Gets a list of deepest estimators."""
        if estimators_ is None:
            estimators_ = []
        if self.level < self.max_level:
            self.left.get_estimators_(estimators_)
            self.right.get_estimators_(estimators_)
        else:
            estimators_.append(self.clf)
        return estimators_


# the following code has been adapted from
# DS20171124a0_tree_based_conditional_distribution_estimation
def plot_pdf(predicted_pdf, target, index=0, scale=1.):
    center, p, width = predicted_pdf
    plt.bar(center * scale, p[:, index], width * scale)
    plt.plot([target[index] * scale] * 2, [0, np.max(p[:, index])],
             'r--',
             label='truth')
    plt.legend()


if __name__ == '__main__':
    from sklearn import datasets
    import matplotlib.pyplot as plt

    # Load the diabetes dataset
    diabetes = datasets.load_diabetes()

    model = ClassifierTree()
    model.fit(diabetes.data, diabetes.target)
    predicted_pdf = model.predict_pdf(diabetes.data)

    instance = 10
    plot_pdf(predicted_pdf, diabetes.target, index=instance)
    plt.title('In-sample prediction of instance {}'.format(instance))
    plt.xlabel('target y')
    plt.savefig('balanced_tree.pdf')
