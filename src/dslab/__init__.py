# -*- coding: utf-8 -*-
from pkg_resources import get_distribution, DistributionNotFound

try:
    # Change here if project is renamed and does not equal the package name
    dist_name = 'DSlab'
    __version__ = get_distribution(dist_name).version
except DistributionNotFound:
    __version__ = 'unknown'

__all__ = ['balanced_tree', 'models', 'feature_selection', 'model_selection', 'visualization']

import sys
print(sys.executable)

import numpy as np
import pandas as pd
import seaborn as sns
sns.set()
import sklearn, matplotlib

print(dict([(m.__name__, m.__version__) for m in [np, pd, sns, sklearn, matplotlib]]))