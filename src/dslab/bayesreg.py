import numpy as np
import pandas as pd
from scipy.stats import norm
from sklearn.base import BaseEstimator, RegressorMixin
from sklearn import linear_model
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import PolynomialFeatures
from sklearn.utils.validation import check_X_y


def rms(w, x, t):
    """Returns root mean square error of a polynomial model.
    
    Args:
        w (iterable): Polynomial coefficients.
        x (iterable): Samples.
        t (iterable): Target values with same shape as x. 

    Returns:
        float: Root mean square error.
    """
    y = np.poly1d(w)(x)
    loss = 0.5 * np.sum((y - t)**2)
    return np.sqrt(2 * loss / x.size)


class BayesianPolynomialRegression(BaseEstimator, RegressorMixin):
    """A Bayesian linear regression with Gaussian prior.

    Bishop (2006): Pattern Recognition and Machine Learning. Springer, New York, Chap. 1

    Parameters:
        M (int, optional): Complexity with respect to order of 
            polynomials.
        alpha (float, optional): Regularization parameter.
    """

    def __init__(self, M=10, alpha=1e-3):
        self.M = M  # Complexity with respect to order of polynomials
        self.alpha = alpha  # Precision of weights

    def fit(self, X, y):
        """Fitting function for Bayesian linear regression with Gaussian prior.

        Args:
            X (array-like or sparse matrix): The training input samples.
                Of shape [n_samples, 1].
            y (array-like): The target values.
                Class labels in classification real numbers in regression. 
                Of shape [n_samples].

        Returns:
            object: Returns self.
        """
        X, y = check_X_y(X, y)
        N = X.shape[0]

        xs = X[:, 0]
        ts = y

        M = self.M
        b_i = np.vectorize(lambda i: np.sum(ts * xs**i))
        A_ij = np.vectorize(lambda i, j: np.sum(xs**(i + j)))
        b = np.fromfunction(b_i, (M + 1, ))
        A = np.fromfunction(A_ij, (M + 1, M + 1))
        S = lambda alpha: A + alpha * np.identity(M + 1)
        w_reg = np.vectorize(lambda alpha: np.linalg.solve(S(alpha), b)[::-1])

        # Estimates mean
        alpha = self.alpha
        m = np.vectorize(lambda x: np.poly1d(w_reg(alpha))(x))
        self.m = m

        # Computes optimal precision
        beta_ML = (N - 1) / (N * rms(w_reg(alpha), xs, ts)**2)

        phi = lambda x: x**np.arange(M + 1)
        # Estimate variance
        S_inv = lambda alpha: beta_ML * A + alpha * np.identity(M + 1)
        S_N = np.linalg.inv(S_inv(alpha))
        s2 = np.vectorize(lambda x: 1. / beta_ML + phi(x).T.dot(S_N).dot(phi(x)))
        self.s2 = s2

        # Returns the estimator
        return self

    def predict_ppd(self, X):
        """Estimate posterior predictive distributions.

        Args:
            X (array-like of shape = [n_samples, n_features]):
                The input samples.

        Returns:
            y (array of shape = [n_samples]):
                Returns :math:`x^2` where :math:`x` is the first column of `X`.
        """
        m = self.m
        s2 = self.s2
        if type(X) == pd.core.DataFrame:
            x = X.values[:, 0]
        else:
            x = X[:, 0]
        polyfit_cdf = lambda x: norm(loc=m(x), scale=np.sqrt(s2(x)))

        try:
            result = [polyfit_cdf(a) for a in x]
        except TypeError:  # x is not iterable
            result = [polyfit_cdf(x)]
        return result

    def predict(self, X):
        """Predict function for estimator. 

        Args:
            X (array-like of shape = [n_samples, n_features]):
                The input samples.

        Returns:
            y (array of shape = [n_samples])
                Returns :math:`x^2` where :math:`x` is the first column of `X`.
        """
        ppds = self.predict_ppd(X)
        result = np.array(list(map(lambda pdf: pdf.mean(), ppds)))
        return result


class BayesianLinearRegression(BaseEstimator, RegressorMixin):
    """A Bayesian linear regression with Gaussian prior.

    Differs from BayesianPolynomialRegression in that the feature
    matrix must be passed explicitly. 

    Bishop (2006): Pattern Recognition and Machine Learning. Springer, New York, Chap. 3

    Parameters:
        alpha (float, optional): Precision of weights
        fit_intercept (bool, optional): Whether to calculate with an
            intercept.
    """
    def __init__(self, alpha=1e-3, fit_intercept=True):
        self.alpha = alpha
        self.fit_intercept = fit_intercept

    def fit(self, X, y):
        """Fitting function for Bayesian linear regression with Gaussian prior.

        Args:
            X (array-like or sparse matrix): The training input feature matrix.
                Of shape [n_samples, n_feats]).
            y (array-like): The target values. 
                Targets are class labels in classification, real numbers 
                in regression. Of shape [n_samples].

        Returns:
            object: Returns self.
        """
        X, y = check_X_y(X, y)

        # Compute optimal weights for regularised linear regression
        # Andreas does polynomial expansion; we'll do that outside this code
        # Implement eqn (3.28) in Bishop, with phi = X
        # NOTE: Here, we treat Bishop's lambda and alpha as the same
        alpha = self.alpha
        N, M = np.shape(X)

        w_reg = np.dot(
            np.dot(np.linalg.inv(alpha * np.identity(M) + np.dot(X.T, X)),
                   X.T), y)

        self.w_reg = w_reg

        # Compute optimal precision from estimated inverse variance
        self.beta_ML = (N - 1) / np.sum((y - np.sum(w_reg * X, axis=1))**2)

        # Matrix S, according to Bishop's definition.
        # Used for mean and variance calculations.
        S_inv = alpha * np.identity(M) + self.beta_ML * np.dot(X.T, X)
        S = np.linalg.inv(S_inv)
        self.S = S

        return self

    def predict_ppd(self, X):
        """Estimate posterior predictive distributions.

        Args:
            X (array-like): The input samples.
                Of shape [n_samples, n_features]. 

        Returns:
            y (array): Returns \\[x^2]\\ where \\[x]\\ is the first column 
                of \\[X]\\. Of shape = [n_samples]. 
        """
        w_reg = self.w_reg
        beta_ML = self.beta_ML
        S = self.S

        # Mean and variance
        m = np.dot(X, w_reg)
        s2 = beta_ML**(-1) + np.diagonal(np.dot(np.dot(X, S), X.T))

        # Fit each sample to a normal dist
        num_samples = X.shape[0]
        normals = [
            norm(loc=m[i], scale=np.sqrt(s2[i]))
            for i in np.arange(0, num_samples)
        ]
        return normals

    def predict(self, X):
        """Predict function for estimator. 

        Args:
            X (array-like of shape = [n_samples, n_features]):
                The input samples.

        Returns:
            y (array of shape = [n_samples])
                Returns :math:`x^2` where :math:`x` is the first column of `X`.
        """
        ppds = self.predict_ppd(X)
        result = np.array([pdf.mean() for pdf in ppds])
        return result

class BayesianRidge(linear_model.BayesianRidge):
    def predict_ppd(self, X):
        μ, σ = self.predict(X, return_std=True)
        ppds = [norm(loc=m, scale=std) for m, std in zip(μ, σ)]
        return ppds

class ARDRegression(linear_model.ARDRegression):
    def predict_ppd(self, X):
        μ, σ = self.predict(X, return_std=True)
        ppds = [norm(loc=m, scale=std) for m, std in zip(μ, σ)]
        return ppds