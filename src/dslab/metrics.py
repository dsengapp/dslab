import numpy as np
import scipy.integrate as integrate
from scipy.stats import norm

def norm_crps(y, est):
    """ Continuous Ranked Probability Score (CRPS) for posterior predictive distributions given as normal distributions

    The CRPS of a Normal distribution $\mathcal{N}\left(\mu,\sigma^2\right)$ with
    mean $\mu$ and standard deviation $\sigma$ and observed value $y$ is given as
   \[
   \text{crps}\left(F\left(\mu,\sigma\right),
   y\right)
   =\sigma\times\left(\frac{y-\mu}{\sigma}\left[2\Phi\left(\frac{y-\mu}{\sigma}\right)-1\right]
   +2\varphi\left(\frac{y-\mu}{\sigma}\right)-\frac{1}{\sqrt{\pi}}\right),
   \]
   where
   $\mathcal{N}\left(\mu,\sigma^2\right)=\varphi\left(\frac{y-\mu}{\sigma}\right)$ and
   $F\left(\mu,\sigma\right)=\Phi\left(\frac{y-mu}{\sigma}\right)$.
   Here, $\varphi$ and $\Phi$ denote the PDF and CDF, respectively, of the normal distribution with mean 0 and variance 1 evaluated at the normal prediction error $(y-\mu)/\sigma$. The average score is computed by
   \[
   \text{CRPS} = \frac{1}{n}\sum\limits_{i=1}^n\text{crps}\left(F_i,y_i\right).
   \]

   Tilmann Gneiting, Adrian E. Raftery, Anton H. Westveld III, and Tom Goldman. Calibrated probabilistic forecasting
   using ensemble model output statistics and mini- mum CRPS estimation. Monthly Weather Review, 133(5):p. 1102, 2005.
     """
    std_err = (y - est.mean()) / est.std()
    return est.std() * (std_err * (2 * norm.cdf(std_err) - 1) +
                        2 * norm.pdf(std_err) - 1 / np.sqrt(np.pi)
                        )

def universal_crps(y, est):
    loss = lambda t: (est.cdf(t) - np.heaviside(t - y, 1)) ** 2
    return integrate.quad(loss, -np.inf, np.inf)[0]

def crps(y_obs, ppd, norm=universal_crps):
    """ Continuous Ranked Probability Score (CRPS) for posterior predictive distributions given as normal distributions
    """
    average_score = np.mean([norm(y, est) for y, est in zip(y_obs, ppd)])
    return average_score