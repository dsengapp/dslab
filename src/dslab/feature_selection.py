from sklearn.base import BaseEstimator, TransformerMixin

class DataFrameSelector(BaseEstimator, TransformerMixin):
    """
    DataFrameSelector as implemented in Géron (2017), p. 68

    Géron, A. (2017): Hands-On Machine Learning with Scikit-Learn and TensorFlow. O’Reilly Media, 2017
    """
    def __init__(self, attribute_names):
        self.attribute_names = attribute_names

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        return X[self.attribute_names]