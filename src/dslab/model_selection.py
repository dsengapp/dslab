"""
10x repeated 10-fold cross-validation for regression problems.

Adapted from algorithm evaluation strategy from

    Bagnall, A.; Lines, J.; Bostrom, A.; Large, J. & Keogh, E.
    The Great Time Series Classification Bake Off: A Review and Experimental Evaluation of Recent Algorithmic Advances
    Data Mining and Knowledge Discovery, 2016
"""

import numpy as np
import pandas as pd
from sklearn.base import clone
from sklearn.utils.estimator_checks import check_estimator
from sklearn.externals import joblib
from sklearn.model_selection import BaseCrossValidator, RepeatedKFold, RepeatedStratifiedKFold


def train_test_fold(model_template, X, y, train_index, test_index):
    """
    Train and test machine learning model for specific train-test-split.

    :param model_template: sklearn conform estimator
    :param X: pandas.DataFrame or numpy.array
    :param y: pandas.Series or one-dimensional numpy.array
    :param train_index: integer array
    :param test_index:  integer array
    :return: trained_model, y_pred pandas.Series
    """
    #The following line is commented, because there is a problem with the DataFrameSelector
    #assert check_estimator(model_template)
    assert isinstance(X, pd.DataFrame)
    assert isinstance(y, pd.Series)
    # Adapted from https://stackoverflow.com/questions/934616/how-do-i-find-out-if-a-numpy-array-contains-integers
    assert np.issubclass_(train_index.dtype.type, np.integer)
    assert np.issubclass_(test_index.dtype.type, np.integer)

    X_train = X.iloc[train_index]
    y_train = y.iloc[train_index].values
    X_test = X.iloc[test_index]

    model = clone(model_template)
    model.fit(X_train, y_train)
    prediction = pd.Series(model.predict(X_test),
                           index=X.index[test_index])
    return model, prediction


def pickle_folds(filename, X, groups=None,
                 cv=RepeatedKFold(n_splits=10, n_repeats=10, random_state=19)):
    """
    Pickle dataset specific train-test folds.

    The pickled object is a dictionary. Its key enumerates the fold-number.
    Each fold is represented by a dictionary with keys "train" and "test",
    which hold the index arrays.

    The folds have been stored as dictionaries in order to allow for simple merging
    of folds evaluated on a distributed system.
    """
    # The following code has been adapted from
    # https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.RepeatedKFold.html
    folds = dict()
    train = list()
    test = list()

    for index, (train_index, test_index) in enumerate(cv.split(X, groups)):
        folds[index] = {'train': train_index,
                        'test': test_index}
        train.append(train_index)
        test.append(test_index)

    joblib.dump(folds, filename)
    import os.path
    return {'filepath': os.path.join(os.getcwd(), filename),
            'filesize': os.stat(filename).st_size
            }

def pickle_stratified_folds(filename, X, target, groups=5,
                            cv=RepeatedStratifiedKFold(n_splits=10,
                                                         n_repeats=10,
                                                         random_state=19)):
    """
    Pickle dataset specific train-test folds, which are stratified.
    Assuming a regression problem, the groups will be the quintiles of the target variable by default.

    The pickled object is a dictionary. Its key enumerates the fold-number.
    Each fold is represented by a dictionary with keys "train" and "test",
    which hold the index arrays.

    The folds have been stored as dictionaries in order to allow for simple merging
    of folds evaluated on a distributed system.
    """
    group_vector = pd.qcut(X[target], groups, labels=False)
    return pickle_folds(filename, X, group_vector.values, cv=cv)

class UnpickleFolds(BaseCrossValidator):
    """
    Unpickle dictionary with train-test folds from file.
    The returned object is a BaseCrossValidator and can be used as folds-generator in sklearn.model_selection.

    Its folds attribute is a dictonary, whose key enumerates the fold-number.
    Each fold is represented by a dictionary with keys "train" and "test",
    which hold the index arrays.
    """
    def __init__(self, fn):
        self.filename = fn

        # Load pickled folds
        self.folds = joblib.load(self.filename)

    def get_n_splits(self):
        return len(self.folds.keys())

    def split(self, X, y=None, groups=None):
        for i in range(self.get_n_splits()):
            yield (self.folds[i]['train'],
                   self.folds[i]['test'])