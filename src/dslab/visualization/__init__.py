
__all__ = ['visualize', 'copula', 'bayes']

from . import visualize
from . import copula

from collections import OrderedDict
from itertools import cycle
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
import os
import seaborn as sns

uoa_colours = OrderedDict([
        ('Dark blue', '#00467F'),
        ('Light blue', '#009AC7'),
        ('Silver', '#8D9091')
    ])
uoa_faculty_colours = OrderedDict([
        ('Arts', '#A71930'),
        ('Business', '#7D0063'),
        ('Creative Arts and Industries', '#D2492A'),
        ('Education and Social Work', '#55A51C'),
        ('Engineering', '#4F2D7F'),
        ('Auckland Law School', '#005B82'),
        ('Medical and Health Sciences', '#00877C'),
        ('Science', '#0039A6')
    ])

lines = ["-", "--", "-.", ":"]
linecycler = lambda: cycle(lines)

def faculty_color_palette(faculty='Engineering'):
    faculties = list(uoa_faculty_colours.keys())
    assert faculty in faculties
    faculties.insert(0, faculties.pop(faculties.index(faculty)))
    colors = uoa_colours.copy()
    for faculty in faculties:
        colors[faculty] = uoa_faculty_colours[faculty]
    return [color for name, color in colors.items()]

fcp = faculty_color_palette()
colorcycler = lambda: cycle(fcp)

def palette(colors=fcp):
    sns.palplot(sns.color_palette(colors))
    plt.show()

def figure(width=4, height=4 * 0.618):
    fig = plt.figure(figsize=(width, height))
    gs = fig.add_gridspec(1, 1)

    style = sns.axes_style("darkgrid")
    with style:
        ax = fig.add_subplot(gs[0, 0])

    return fig, ax

class Savefig(object):
    def __init__(self, destination_folder='.'):
        self.dest = destination_folder

    def __call__(self, filename='Untitled.pdf', fig=None):
        """
        Save figure and close figure window
        :param filename: Filename of the figure. Its suffix defines the filetype. Default ('Unititled.pdf')
        :param fig: Figure object to saved, if None (default) latest figure is saved
        :return:
        """
        file_path = os.path.join(self.dest, filename)

        savefig = lambda obj: obj.savefig(file_path, bbox_inches='tight')
        if fig is None:  # save and close latest figure
            savefig(plt)
            plt.close()
        else:  # save and close specific figure
            savefig(fig)
            plt.close(fig)
        return file_path

