import numpy as np
import matplotlib.pyplot as plt

from dslab.bayesreg import BayesianPolynomialRegression

def plot_posterior_predictive(ppd, x, hdi_pro=0.95, signal=None):
    """
    Plot posterior predictive distribution

    :param ppd: List of frozen rv_continuous objects
    :param x: independent variable
    :param hdi_pro: probability of highest density interval (default=0.95)
    :param signal: dependent variable
    :return:
    """
    m = np.array([p.mean() for p in ppd])
    q_low = np.array([p.ppf((1. - hdi_pro) / 2) for p in ppd])
    q_up = np.array([p.ppf((1. + hdi_pro) / 2) for p in ppd])

    line = plt.plot(x, m, linestyle='--', label='point prediction')
    plt.fill_between(x, q_low, q_up, alpha=0.2,
                     color=line[0].get_color(), label='95% HDI')

    if not signal is None:
        plt.plot(x, signal, label='signal')

def regression(x, y):
    """
    Plot observations y as function of independent variable x using Bayesian Polynomial Regression
    :param x: samples of independent variable
    :param y: samples of dependent variable
    """
    poly_reg = BayesianPolynomialRegression()
    if len(x.shape) == 1:
        X = x[:, np.newaxis]
    else:
        X = x

    poly_reg.fit(X, y)
    x_cont = np.linspace(np.min(x), np.max(x), 200)
    ppd = poly_reg.predict_ppd(x_cont[:, np.newaxis])

    plot_posterior_predictive(ppd, x_cont)
    plt.plot(x, y, 'ro', label='observation')
    plt.xlabel('independent variable x')
    plt.legend()
