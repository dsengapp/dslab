import matplotlib.pylab as plt
import numpy as np
import os
import pandas as pd
import seaborn as sns
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler, QuantileTransformer
from sklearn.decomposition import PCA

def compute_binned_principle_components(X, scale=True, max_pc=3, bins=20,
                                        export_transformer=False):
    """
    Project feature matrix onto its principle components and map to quantiles

    :param X: feature matrix
    :param scale: boolean (default: True)
    :param max_pc: number of returned principle components (default 3)
    :param bins: number of intervals the principle components should be binned into (default 20)
    :return: pd.DataFrame of binned principle components,
             list of bin_boundaries,
             projection onto principle components
    """
    pc_binned = list()
    pc_bins = list()

    if not max_pc==3:
        raise NotImplementedError

    preprocessing_steps = [('pca', PCA(n_components=max_pc, random_state=42)),# Get principle components
                           ('qt', QuantileTransformer())]
    if scale:
        preprocessing_steps.insert(0, ('normalize', StandardScaler()))
    preprocessing = Pipeline(preprocessing_steps)

    X_pca = preprocessing.fit_transform(X)

    pc = pd.DataFrame(X_pca, columns=list(range(max_pc)))

    for col in range(max_pc):
        out, intervals = pd.cut(pc[col],
                                np.linspace(0, 1, bins+1),
                                retbins=True)
        pc_binned.append(out)
        pc_bins.append(intervals)

    df = pd.concat(pc_binned, axis=1)

    result = [df, pc_bins, X_pca]
    if export_transformer:
        result.append(preprocessing)
    return result

def binned_class_frequency(X, y, scale=True, max_pc=3, bins=20):
    """
    Count class frequencies for binned features projected onto principle components

    :param X: feature matrix
    :param y: target vector
    :param scale: boolean (default: True)
    :param max_pc: number of returned principle components (default 3)
    :param bins: number of intervals the principle components should be binned into (default 20)

    :return: dictionary of dictionaries containing DataFrames with binned class frequency
             First dictionary indexes by principle components, second by class labels
    """
    if not max_pc==3:
        raise NotImplementedError

    df, pc_bins, _ = compute_binned_principle_components(X, scale=scale, max_pc=max_pc, bins=bins)
    counts = dict()

    for target in np.unique(y):
        mask = y==target
        for pc_a, pc_b in [(0, 1), (0, 2), (1, 2)]:
            frequencies = df[mask].groupby([pc_a, pc_b]).count().reset_index()
            frequencies.rename({frequencies.columns[-1]: 'counts'},
                               axis=1, inplace=True)
            projection = (pc_a, pc_b)

            if not projection in counts.keys():
                counts[projection] = dict()

            counts[projection][target] = frequencies.pivot(pc_a, pc_b, 'counts').fillna(value=0)

    return counts

def compute_copula(X, y, bins=20):
    """
        Compute copula of principle components for multi-class classification problem with 3 classes

        :param X: feature matrix
        :param y: target vector containing three different labels
        :param bins: number of intervals the principle components should be binned into (default 20)

        :return: dictionary of DataFrames indexed over combinations of principle components

        The copula for an individual slice can be retrieved by

        >>> copula = compute_copula(X, y)
        >>> copula[(0,1)].loc[slice('EAP'), :]
    """

    ratios = dict()
    counts = binned_class_frequency(X, y, bins=bins)

    labels = np.unique(y)

    for projection, conditional_counts in counts.items():

        # Stack DataFrames with class names becomming a hierarchical index
        stacked_tables = pd.concat([conditional_counts[label] for label in labels],
                                   keys=labels)

        # Sum samples per bin over class labels
        total = stacked_tables.sum(level=1)

        # Compute ratios per bin
        ratios[projection] = stacked_tables / total
        ratios[projection].fillna(value=0, inplace=True)  # Remove division by zeros
        # sort index in order to have negative values in lower left quadrant
        ratios[projection].sort_index(ascending=False,
                                      inplace=True)

    return ratios

def _get_slice(df, label, values=True):
    slice = df.loc[pd.IndexSlice[[label], :],
                  pd.IndexSlice[:]]
    if values:
        return slice.values
    else:
        return slice

def _get_bin_index(x, bin_boundaries):
    is_smaller = x > np.array(bin_boundaries)
    return np.max(np.cumsum(is_smaller))

def locate_bin(copula, row_cont, col_cont):
    N = copula.shape[1]

    # Collect right interval borders from row intervals
    top_to_bottom = [copula.index.levels[1][col].right
                     for col in copula.index.labels[1][:N]]
    # Collect right interval border from column intervals
    left_to_right = [col.right for col in copula.columns]

    row_index = N - 1 - _get_bin_index(row_cont, top_to_bottom)
    col_index = _get_bin_index(col_cont, left_to_right)
    return row_index, col_index


def plot_copula(copula, order, examples=[]):
    N = copula.shape[1]

    col_label = [str(col) for col in copula.columns]
    row_label = [str(copula.index.levels[1][col]) for col in copula.index.labels[1][:N]]

    fig = np.stack([_get_slice(copula, order[c]) for c in range(3)],
                   axis=-1)

    sns.set_style("whitegrid", {'axes.grid': False})
    plt.imshow(fig)
    ax = plt.gca()
    ax.set_xticks(np.arange(0, N))
    plt.xticks(rotation=90)
    ax.set_yticks(np.arange(0, N))
    ax.set_xticklabels(col_label)
    ax.set_yticklabels(row_label)

    if len(examples) > 0:
        for example in examples:
            x, y = example[1]
            row, column = locate_bin(copula, x, y)
            plt.plot([column], [row], example[0])

def copula_legend(label):
    path = os.path.dirname(os.path.abspath(__file__))
    output_fn = os.path.join(path, 'copula_legend_labels.tex')
    output_pdf = os.path.join(path, 'copula_legend_template.pdf')
    if os.path.exists(output_pdf):
        os.remove(output_pdf)

    macro = ['PCa', 'PCb', 'PCc']

    table = dict([(macro[i], label[i]) for i in range(3)])

    with open(output_fn, 'w') as output:
        for key in table.keys():
            output.write('\\renewcommand{{\\{}}}{{{}}}\n'.format(key, table[key]))

    os.system("cd {}; pdflatex copula_legend_template.tex".format(path))

    return output_pdf
